# Program:      SerialMonitor Python (Arduino Like)
# Author:       Elvis Baketa
# Device:       
# Version:      

import sys
from PyQt6.QtCore import QIODevice, QTimer, QByteArray
from PyQt6.QtSerialPort import QSerialPort, QSerialPortInfo
from PyQt6.QtWidgets import QApplication, QMainWindow, QMessageBox, QMenuBar, QMenu, QVBoxLayout, QHBoxLayout, QWidget, QTextEdit, QPushButton, QLineEdit, QComboBox, QLabel, QCheckBox, QToolBar
from PyQt6.QtGui import QAction, QTextCursor

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.window_gui()
        self.main()

    def window_gui(self):
        self.setWindowTitle("Serial Monitor")

        # Set fixed window size
        self.setFixedSize(800, 600)

        # Central widget
        central_widget = QWidget()
        self.setCentralWidget(central_widget)

        # Layout
        layout = QVBoxLayout(central_widget)

        # Create a layout for the connection settings
        connectionLayout = QHBoxLayout()
 
        # ComboBox to list available ports
        self.port_combo = QComboBox()
        connectionLayout.addWidget(self.port_combo)
        
        # Populate ComboBox with available ports
        self.list_ports()

        # ComboBox to list available baud rates
        self.baud_combo = QComboBox()
        connectionLayout.addWidget(self.baud_combo)
        
        # Populate ComboBox with available baud rates
        self.list_baud_rates()

        # Button to refresh port list
        self.refresh_button = QPushButton("Refresh")
        self.refresh_button.clicked.connect(self.refresh_port_list)
        connectionLayout.addWidget(self.refresh_button)

        # Button to connect to selected port
        self.connect_button = QPushButton("Connect")
        self.connect_button.clicked.connect(self.connect_serial_port)
        connectionLayout.addWidget(self.connect_button)

        # Button to disconnect to selected port
        self.disconnect_button = QPushButton("Disconnect")
        self.disconnect_button.clicked.connect(self.disconnect_serial_port)
        connectionLayout.addWidget(self.disconnect_button)

        # Add the read layout to the main layout
        layout.addLayout(connectionLayout)



        # Create a layout for the send outgoing data
        sendLayout = QHBoxLayout()

        # LineEdit to enter text to send
        self.line_edit = QLineEdit()
        sendLayout.addWidget(self.line_edit)

        # Button to send text
        self.send_button = QPushButton("Send")
        self.send_button.clicked.connect(self.send_data)
        sendLayout.addWidget(self.send_button)

        # Add the send layout to the main layout
        layout.addLayout(sendLayout)



        # Create a layout for the read incoming data
        readLayout = QVBoxLayout()

        # TextEdit to display serial data
        self.text_edit = QTextEdit()
        self.text_edit.setReadOnly(True)
        layout.addWidget(self.text_edit)

        # Add the read layout to the main layout
        layout.addLayout(readLayout)



        # Create a layout for the send outgoing data
        sendOptionsLayout = QHBoxLayout()

        # Add a checkbox to the main layout
        self.autoscroll_check_box = QCheckBox("Autoscroll")
        self.autoscroll_check_box.setObjectName("autoscroll_check_box")
        self.autoscroll_check_box.setChecked(True)
        # Set the font size of the button
        # self.autoscrollCheckBox.stateChanged.connect(self.checkbox_toggled)
        sendOptionsLayout.addWidget(self.autoscroll_check_box)

        # Add a combo box to the main layout
        self.line_ending_combo = QComboBox()
        # Set the font size of the button
        self.line_ending_combo.addItems(["No Line Ending", "Newline"])
        sendOptionsLayout.addWidget(self.line_ending_combo)

        # Button to disconnect to selected port
        self.clear_recived_data_button = QPushButton("Clear")
        self.clear_recived_data_button.clicked.connect(self.clearReceivedData)
        sendOptionsLayout.addWidget(self.clear_recived_data_button)

        # Label to display connection status
        self.connection_status_label = QLabel("Disconnected")
        self.connection_status_label.setObjectName("connection_status_label")
        sendOptionsLayout.addWidget(self.connection_status_label)

        sendOptionsLayout.addStretch()

        # Add the send layout to the main layout
        layout.addLayout(sendOptionsLayout)



         # Connect Enter Pressed Signal
        self.line_edit.returnPressed.connect(self.send_data)

        # Serial port setup
        self.serial = QSerialPort()
        self.serial.readyRead.connect(self.read_data)



    def list_ports(self):
        # Clear the combo box before adding new items
        self.port_combo.clear()
        
        # Get a list of available ports
        available_ports = QSerialPortInfo.availablePorts()

         # Extract port names and sort them
        port_names = sorted([port.portName() for port in available_ports])

        # Add sorted port names to the combo box
        for port_name in port_names:
            self.port_combo.addItem(port_name)

        # Set the first available port as default
        if port_names:
            self.port_combo.setCurrentIndex(0)

    def list_baud_rates(self):
        baud_rates = QSerialPortInfo.standardBaudRates()
        for baud_rate in baud_rates:
            self.baud_combo.addItem(str(baud_rate))
        # Set 9600 as the default baud rate
        self.baud_combo.setCurrentText("9600")  

    def refresh_port_list(self):
        self.port_combo.clear()
        self.list_ports()

    def connect_serial_port(self):
        selected_port = self.port_combo.currentText()
        selected_baud_rate = int(self.baud_combo.currentText())
        if selected_port:
            self.serial.setPortName(selected_port)
            self.serial.setBaudRate(selected_baud_rate)
            self.serial.setDataBits(QSerialPort.DataBits.Data8)
            self.serial.setParity(QSerialPort.Parity.NoParity)
            self.serial.setStopBits(QSerialPort.StopBits.OneStop)
            self.serial.setFlowControl(QSerialPort.FlowControl.NoFlowControl)
            if self.serial.open(QIODevice.OpenModeFlag.ReadWrite):
                self.connection_status_label.setText(f"Connected to {selected_port} at {selected_baud_rate} baud")
            else:
                self.connection_status_label.setText(f"Failed to connect to {selected_port}")

    def clearReceivedData(self):
        self.text_edit.clear()

    def disconnect_serial_port(self):
        if self.serial.isOpen():
            self.serial.close()
            self.connection_status_label.setText("Disconnected")

    def read_data(self):
        if self.serial.isOpen() and self.serial.bytesAvailable():
            text = self.serial.readLine().data().decode('utf-8').strip()
            self.text_edit.append(text)

    def send_data(self):
        text = self.line_edit.text()
        if text:
            # Ensure the text is encoded properly
            try:
                self.serial.write(text.encode('utf-8'))
                self.line_edit.clear()
            except Exception as e:
                self.connection_status_label.setText(f"Error sending data: {e}")

    def main(self):
        pass

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    with open("themes/style.qss", "r") as f:
        _style = f.read()
        app.setStyleSheet(_style)
    window.show()
    sys.exit(app.exec())
